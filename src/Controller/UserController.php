<?php


namespace App\Controller;


use App\Entity\User;
use App\Form\UserRegistrationForm;
use App\Security\LoginFormAuthenticator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class UserController extends Controller
{

    /**
     * @var LoginFormAuthenticator
     */
    private $authenticator;

    public function __construct(LoginFormAuthenticator $authenticator)
    {

        $this->authenticator = $authenticator;
    }

    /**
     * @Route("/register", name="user_register")
     */
    public function registerAction(Request $request, GuardAuthenticatorHandler $guardAuthenticatorHandler)
    {
        $form = $this->createForm(UserRegistrationForm::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() and $form->isValid()) {
            /**
             * @var $user User
             */
            $user = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'Welcome ' . $user->getEmail());

            return $guardAuthenticatorHandler
                ->authenticateUserAndHandleSuccess(
                    $user,
                    $request,
                    $this->authenticator,
                    'main'
                );
        }


        return $this->render('user/register.html.twig', [
            'form' => $form->createView()
        ]);
    }

}
